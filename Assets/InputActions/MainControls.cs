//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.3.0
//     from Assets/InputActions/MainControls.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @MainControls : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @MainControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MainControls"",
    ""maps"": [
        {
            ""name"": ""MainActionMap"",
            ""id"": ""a7b9ff75-f2d8-49f6-9715-39a092f05811"",
            ""actions"": [
                {
                    ""name"": ""MouseClick"",
                    ""type"": ""Value"",
                    ""id"": ""895cf0c4-fafb-49a9-9052-57cf1b54091e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""CameraMovement"",
                    ""type"": ""Value"",
                    ""id"": ""c2f60884-0a22-4202-962e-d445f6c56991"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""CameraRotation"",
                    ""type"": ""Value"",
                    ""id"": ""050ecd5b-7845-4153-bf0d-d40ad6e04c68"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""CameraZoom"",
                    ""type"": ""PassThrough"",
                    ""id"": ""347c966a-cbf6-4be8-9b90-3268ae0ba7f8"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Testing"",
                    ""type"": ""Value"",
                    ""id"": ""4ab10511-9ce7-47a2-944a-7bbefda79238"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""MouseClickRight"",
                    ""type"": ""Value"",
                    ""id"": ""20613bfe-3c63-41c5-bb14-a1959b8c1be9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2ad75fa1-855b-4441-a86a-20cd49b99495"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""30953512-a04e-443a-99d1-7332fd5b88fe"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d6df5daf-9c2f-4007-bdd6-dfbf6f6212a0"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""ac4f5898-86aa-4355-a84d-bdaa734232de"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""209e19d8-3fcb-4d26-8180-c7f357fe88ee"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""01aab8e1-e33f-4fe0-bcad-a562832fa762"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""b7b8e86c-9e8e-4768-b9ba-fa25f1696cd9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f86b5cc5-b753-4db5-a190-63e113da1d1f"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""37ba70e8-dfc6-4954-88f3-46143abbc290"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraRotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b2c0e78f-9dc8-447f-8beb-a585e87c9983"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraZoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9fd5f7dd-e8f7-4658-8615-1bb9b403f11e"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Testing"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c02397c1-4c56-4dcd-871b-697da0e036eb"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseClickRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // MainActionMap
        m_MainActionMap = asset.FindActionMap("MainActionMap", throwIfNotFound: true);
        m_MainActionMap_MouseClick = m_MainActionMap.FindAction("MouseClick", throwIfNotFound: true);
        m_MainActionMap_CameraMovement = m_MainActionMap.FindAction("CameraMovement", throwIfNotFound: true);
        m_MainActionMap_CameraRotation = m_MainActionMap.FindAction("CameraRotation", throwIfNotFound: true);
        m_MainActionMap_CameraZoom = m_MainActionMap.FindAction("CameraZoom", throwIfNotFound: true);
        m_MainActionMap_Testing = m_MainActionMap.FindAction("Testing", throwIfNotFound: true);
        m_MainActionMap_MouseClickRight = m_MainActionMap.FindAction("MouseClickRight", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // MainActionMap
    private readonly InputActionMap m_MainActionMap;
    private IMainActionMapActions m_MainActionMapActionsCallbackInterface;
    private readonly InputAction m_MainActionMap_MouseClick;
    private readonly InputAction m_MainActionMap_CameraMovement;
    private readonly InputAction m_MainActionMap_CameraRotation;
    private readonly InputAction m_MainActionMap_CameraZoom;
    private readonly InputAction m_MainActionMap_Testing;
    private readonly InputAction m_MainActionMap_MouseClickRight;
    public struct MainActionMapActions
    {
        private @MainControls m_Wrapper;
        public MainActionMapActions(@MainControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @MouseClick => m_Wrapper.m_MainActionMap_MouseClick;
        public InputAction @CameraMovement => m_Wrapper.m_MainActionMap_CameraMovement;
        public InputAction @CameraRotation => m_Wrapper.m_MainActionMap_CameraRotation;
        public InputAction @CameraZoom => m_Wrapper.m_MainActionMap_CameraZoom;
        public InputAction @Testing => m_Wrapper.m_MainActionMap_Testing;
        public InputAction @MouseClickRight => m_Wrapper.m_MainActionMap_MouseClickRight;
        public InputActionMap Get() { return m_Wrapper.m_MainActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MainActionMapActions set) { return set.Get(); }
        public void SetCallbacks(IMainActionMapActions instance)
        {
            if (m_Wrapper.m_MainActionMapActionsCallbackInterface != null)
            {
                @MouseClick.started -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnMouseClick;
                @MouseClick.performed -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnMouseClick;
                @MouseClick.canceled -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnMouseClick;
                @CameraMovement.started -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraMovement;
                @CameraMovement.performed -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraMovement;
                @CameraMovement.canceled -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraMovement;
                @CameraRotation.started -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraRotation;
                @CameraRotation.performed -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraRotation;
                @CameraRotation.canceled -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraRotation;
                @CameraZoom.started -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraZoom;
                @CameraZoom.performed -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraZoom;
                @CameraZoom.canceled -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnCameraZoom;
                @Testing.started -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnTesting;
                @Testing.performed -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnTesting;
                @Testing.canceled -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnTesting;
                @MouseClickRight.started -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnMouseClickRight;
                @MouseClickRight.performed -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnMouseClickRight;
                @MouseClickRight.canceled -= m_Wrapper.m_MainActionMapActionsCallbackInterface.OnMouseClickRight;
            }
            m_Wrapper.m_MainActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MouseClick.started += instance.OnMouseClick;
                @MouseClick.performed += instance.OnMouseClick;
                @MouseClick.canceled += instance.OnMouseClick;
                @CameraMovement.started += instance.OnCameraMovement;
                @CameraMovement.performed += instance.OnCameraMovement;
                @CameraMovement.canceled += instance.OnCameraMovement;
                @CameraRotation.started += instance.OnCameraRotation;
                @CameraRotation.performed += instance.OnCameraRotation;
                @CameraRotation.canceled += instance.OnCameraRotation;
                @CameraZoom.started += instance.OnCameraZoom;
                @CameraZoom.performed += instance.OnCameraZoom;
                @CameraZoom.canceled += instance.OnCameraZoom;
                @Testing.started += instance.OnTesting;
                @Testing.performed += instance.OnTesting;
                @Testing.canceled += instance.OnTesting;
                @MouseClickRight.started += instance.OnMouseClickRight;
                @MouseClickRight.performed += instance.OnMouseClickRight;
                @MouseClickRight.canceled += instance.OnMouseClickRight;
            }
        }
    }
    public MainActionMapActions @MainActionMap => new MainActionMapActions(this);
    public interface IMainActionMapActions
    {
        void OnMouseClick(InputAction.CallbackContext context);
        void OnCameraMovement(InputAction.CallbackContext context);
        void OnCameraRotation(InputAction.CallbackContext context);
        void OnCameraZoom(InputAction.CallbackContext context);
        void OnTesting(InputAction.CallbackContext context);
        void OnMouseClickRight(InputAction.CallbackContext context);
    }
}
