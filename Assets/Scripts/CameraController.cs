using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    private const float MIN_FOLLOW_Y_OFFSET = 2f;
    private const float MAX_FOLLOW_Y_OFFSET = 12f;

    [SerializeField] private CinemachineVirtualCamera cinemachineVirtualCamera;
    
    private bool movementPress = false;
    private bool rotationPress = false;
    private Vector2 movementVector;
    private float zoomVector;
    private Vector3 inputMoveDir = Vector3.zero;
    private Vector3 rotationVector = Vector3.zero;    

    public void OnCameraMovement(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            movementVector = context.ReadValue<Vector2>();
            movementPress = true;
        }
        else
        {
            movementPress = false;
        }    
    }

    public void OnCameraRotation(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            rotationVector.y = context.ReadValue<float>();
            rotationPress = true;
        }
        else
        {
            rotationPress = false;
        }
    }

    public void OnCameraZoom(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            zoomVector = context.ReadValue<float>();
            CinemachineTransposer cinemachineTrasposer = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>();            
            Vector3 followOffset = cinemachineTrasposer.m_FollowOffset;

            float zoomAmount = 1f;
            float zoomSpeed = 100f;

            if(zoomVector > 0)
            {
                followOffset.y += zoomAmount;
            }
            else if (zoomVector < 0)
            {
                followOffset.y -= zoomAmount;
            }             

            followOffset.y = Mathf.Clamp(followOffset.y, MIN_FOLLOW_Y_OFFSET, MAX_FOLLOW_Y_OFFSET);          
            cinemachineTrasposer.m_FollowOffset = Vector3.Lerp(cinemachineTrasposer.m_FollowOffset, followOffset, Time.deltaTime * zoomSpeed);
        }        
    }

    private void Update()
    {
        HandleMovement();
        HandleRotation();
    }

    private void HandleMovement()
    {
        if (movementPress)
        {
            inputMoveDir = new Vector3(movementVector.x, 0, movementVector.y);

            float moveSpeed = 10f;
            Vector3 moveVector = transform.forward * inputMoveDir.z + transform.right * inputMoveDir.x;
            transform.position += moveVector * moveSpeed * Time.deltaTime;
        }
    }

    private void HandleRotation()
    {
        if (rotationPress)
        {
            float rotationSpeed = 100f;
            transform.eulerAngles += rotationVector * rotationSpeed * Time.deltaTime;
        }
    }
}
