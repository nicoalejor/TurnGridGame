using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAction : BaseAction
{
    [SerializeField] private int maxMoveDistance = 4;
    
    [SerializeField] private float moveSpeed = 4f;
    [SerializeField] private float rotationSpeed = 10f;

    private Vector3 targetPosition;
    private float stoppingDistance = 0.1f;

    public event EventHandler OnStartMoving;
    public event EventHandler OnStopMoving;

    private List<Vector3> positionList;
    private int currentPositionIndex;
    private void Update()
    {
        if (!isActive) return;

        targetPosition = positionList[currentPositionIndex];
        Vector3 moveDirection = (targetPosition - transform.position).normalized;

        //For rotation changing forward direction, you can save the transform forward before the Lerp since the forward is constantly
        //changing, depending on your purpose 
        transform.forward = Vector3.Lerp(transform.forward, moveDirection, Time.deltaTime * rotationSpeed);

        if (Vector3.Distance(transform.position, targetPosition) > stoppingDistance)
        {
            //For movement           
            transform.position += moveDirection * moveSpeed * Time.deltaTime;
            //For rotation changing rotation using Quaternion.
            //transform.rotation = Quaternion.LookRotation(Vector3.Lerp(transform.forward, moveDirection, Time.deltaTime * rotationSpeed));
        }
        else
        {
            currentPositionIndex++;
            if(currentPositionIndex >= positionList.Count)
            {
                OnStopMoving?.Invoke(this, EventArgs.Empty);
                ActionComplete();
            }            
        }

        
    }
    public override void TakeAction(GridPosition gridPosition, Action onActionComplete)
    {
        List<GridPosition> pathGridPositionList = Pathfinding.Instance.FindPath(unit.GetGridPosition(), gridPosition, out int pathlenght);
        
        currentPositionIndex = 0;
        positionList = new List<Vector3>();

        foreach (GridPosition pathGridPosition in pathGridPositionList)
        {
            positionList.Add(LevelGrid.Instance.GetWorldPosition(pathGridPosition));
        }

        OnStartMoving?.Invoke(this, EventArgs.Empty);

        ActionStart(onActionComplete);
    }   

    public override List<GridPosition> GetValidActionGridPositionList()
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();

        GridPosition unitGridPosition = unit.GetGridPosition();

        for (int x = -maxMoveDistance; x <= maxMoveDistance ; x++)
        {
            for (int z = -maxMoveDistance; z <= maxMoveDistance; z++)
            {
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                //Valid Grid Position
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)) continue;                
                //Same position in the grid
                if (unitGridPosition == testGridPosition) continue;
                //GridPosition occupied by another unit
                if (LevelGrid.Instance.HasAnyUnitGridPosition(testGridPosition)) continue;

                if(!Pathfinding.Instance.IsWalkableGridPosition(testGridPosition)) continue;

                if(!Pathfinding.Instance.HasPath(unitGridPosition, testGridPosition)) continue;

                int pathFindindDistanceMultiplier = 10;
                //Path lenght too long
                if (Pathfinding.Instance.GetPathLenght(unitGridPosition, testGridPosition) > maxMoveDistance * pathFindindDistanceMultiplier) continue;                

                validGridPositionList.Add(testGridPosition);
            }
        }

        return validGridPositionList;
    }

    public override string GetActionName()
    {
        return "Move";
    }

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        int targetCountAtGridPosition = unit.GetAction<ShootAction>().GetTargetCountAtPosition(gridPosition);
        //Prioritze position with more enemies
        int weightForEnemy = 10;

        return new EnemyAIAction
        {
            gridPosition = gridPosition,
            actionValue = targetCountAtGridPosition * weightForEnemy
        };
    }
}
